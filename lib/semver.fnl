#!/usr/bin/env fennel

(local semver {})

(lambda semver.new [{:major ?major :minor ?minor :patch ?patch}]
  {:major (or ?major 0)
   :minor (or ?minor 0)
   :patch (or ?patch 1)}
  )



semver
