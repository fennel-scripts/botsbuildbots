#!/usr/bin/env fennel

(local fennel (require :fennel))

;;(print (fennel.view arg))

(let [proj (require "project")] (print "version:\n" (or (fennel.view proj.botsversion) "please add a botsversion entry to your project.fnl")))

(fn cake [self]
  (print "*that action is not yet implemented*"))

(local defaults
	   {:prepare cake :build cake :install cake
		:uninstall cake :compile cake :build cake
		:prepare cake :test cake :clean cake
		:clean cake :run cake :default cake})

(fn help []
  (print "\tavailable commands")
  (each [key _ (pairs defaults)]
	(print (.. "\t" key))))

(tset defaults :help help)
(tset defaults :default help)
(local bots (require :bots))
(each [k _ (pairs defaults)]
  (when (= (. bots k) nil)
	(tset bots k (. defaults k))))

(: bots (or (. arg 1) :default) arg)
