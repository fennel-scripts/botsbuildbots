#!/usr/bin/env fennel

(local bots {:name "erik"})


(macro target [name args ...]
  '(tset bots ,name
		 (fn ,args
		   (do ,...))))


(target :install [this]
		(print "installing")
		(print this.name))



(bots:install)

nil
